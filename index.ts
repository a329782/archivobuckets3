import * as AWS from 'aws-sdk';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as util from 'util';
import { isModuleNamespaceObject } from 'util/types';
import {uuid} from 'uuidv4';

const readFile = util.promisify(fs.readFile);

const BUCKET_NAME = 'practica-archivo-bucket-s3';

dotenv.config();

const s3 = new AWS.S3({
    region: process.env.AWS_REGION,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
});

const uploadToS3 = async(data: Buffer): Promise<string> => {
    const name = uuid() + '.jpg';
    await s3.putObject({
        Key: name,
        Bucket: BUCKET_NAME,
        ContentType: 'image/jpg',
        Body: data,
        ACL: 'public-read',
    }).promise();
    return `https://${BUCKET_NAME}.s3.amazonaws.com/${name}`;
};

const main = async () => {
    try{
        const data = await readFile('./Cold Mountain.jpg');
        const url = await uploadToS3(data);
        console.log(url);
    } catch (err) {
        console.log(err);
    }
};

main();